package com.jam.controller;

import com.alibaba.fastjson.JSON;
import com.jam.dao.ChartsDao;
import com.jam.dao.UserDao;
import com.jam.pojo.Charts;
import com.jam.pojo.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * @program: sportplay
 * @description: 这是图表展示的控制类
 * @author: Mr.Pu
 * @create: 2021-05-11 21:21
 **/

@CrossOrigin
@RestController
public class ChartsController {

    @Autowired
    ChartsDao chartsDao;

    /**
     * @Description: 使用此方法获取到数据库中有关图表的所有数据
     * @param
     * @Return: java.lang.String
     * @Author: Mr.Pu
     * @Date: 2021/5/12
     */
    @RequestMapping("/kpiView")
    public String getAllData(){
        HashMap<String, Object> data = new HashMap<>();
        List<Charts> charts = chartsDao.getAllData();
        if(charts!=null){
            data.put("data",charts);
            data.put("flag",200);
        }else {
            data.put("flag", 404);
        }
        return JSON.toJSONString(data);
    }


}
