package com.jam.pojo;

/**
 * @program: sportplay
 * @description: 文件上传返回信息实体类
 * @author: Mr.Pu
 * @create: 2021-05-14 21:39
 **/

public class Resp<E> {

    private String code;
    private String message;
    private E body;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public E getBody() {
        return body;
    }

    public void setBody(E body) {
        this.body = body;
    }

    public Resp(String code, String message, E body) {
        this.code = code;
        this.message = message;
        this.body = body;
    }

    public static <E> Resp<E> success(E body){
        return new Resp("200","文件上传成功",body);
    }

    public static <E> Resp<E> fail(String code,String message){
        return new Resp(code,message,(Object)null);
    }
}
