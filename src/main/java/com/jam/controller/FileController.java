package com.jam.controller;

import com.alibaba.fastjson.JSON;
import com.jam.util.impl.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: sportplay
 * @description: 文件上传下载控制层类
 * @author: Mr.Pu
 * @create: 2021-05-14 21:44
 **/

@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping("/upload")
    private String upload(@RequestParam("file") MultipartFile file){
        return JSON.toJSONString(fileUploadService.upload(file));
    }



}
