package com.jam.pojo;

/**
 * @program: sportplay
 * @description: 这是图表信息实体类
 * @author: Mr.Pu
 * @create: 2021-05-12 14:51
 **/

public class Charts {

    private String kpi_name;
    private int kpi_value;

    @Override
    public String toString() {
        return "Charts{" +
                "kpi_name='" + kpi_name + '\'' +
                ", kpi_value=" + kpi_value +
                '}';
    }

    public String getKpi_name() {
        return kpi_name;
    }

    public void setKpi_name(String kpi_name) {
        this.kpi_name = kpi_name;
    }

    public int getKpi_value() {
        return kpi_value;
    }

    public void setKpi_value(int kpi_value) {
        this.kpi_value = kpi_value;
    }

    public Charts(String kpi_name, int kpi_value) {
        this.kpi_name = kpi_name;
        this.kpi_value = kpi_value;
    }

    public Charts() {
    }
}
