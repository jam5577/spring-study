package com.jam.controller;

import com.alibaba.fastjson.JSON;
import com.jam.dao.UserDao;
import com.jam.pojo.QueryInfo;
import com.jam.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
public class UserController {

    @Autowired
    UserDao uDao;

    @RequestMapping("/alluser")
    public String getUserList(QueryInfo queryInfo) {

        int numbers = uDao.getUserCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();
        List<User> users = uDao.getAllUser("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", users);
        return JSON.toJSONString(res);
    }

    @RequestMapping("/updateState")
    public String updateUserState(@RequestParam("id") Integer id, @RequestParam("state") Boolean state) {
        int i = uDao.updateState(id, state);
        return i > 0 ? "success" : "error";
    }

    @RequestMapping("/addUser")
    public String addUser(@RequestBody User user) {
        user.setRole("普通用户");
        user.setState(false);
        int i = uDao.addUser(user);
        return i > 0 ? "success" : "error";
    }

    @RequestMapping("/deleteUser")
    public String deleteUser(int id) {
        int i = uDao.deleteUse(id);
        return i > 0 ? "success" : "error";
    }

    @RequestMapping("/getUpdate")
    public String getUpdateUser(int id) {
        User user = uDao.getUpdateUser(id);
        return JSON.toJSONString(user);
    }

    @RequestMapping("/editUser")
    public String editUser(@RequestBody User user) {
        int i = uDao.editUser(user);
        return i > 0 ? "success" : "error";
    }
}
