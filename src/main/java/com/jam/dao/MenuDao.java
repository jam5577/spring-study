package com.jam.dao;

import com.jam.pojo.MainMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuDao {
    List<MainMenu> getMenu();
}
