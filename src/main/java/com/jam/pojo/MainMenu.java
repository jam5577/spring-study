package com.jam.pojo;

import java.util.List;

//主导航
public class MainMenu {
    private int id;
    private String title;
    private String path;
    private List<SubMenu> List;

    @Override
    public String toString() {
        return "MainMenu{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", List=" + List +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<SubMenu> getList() {
        return List;
    }

    public void setList(List<SubMenu> List) {
        this.List = List;
    }

    public MainMenu() {
    }

    public MainMenu(String title, String path, List<SubMenu> List) {
        this.title = title;
        this.path = path;
        this.List = List;
    }
}
