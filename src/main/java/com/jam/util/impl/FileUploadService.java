package com.jam.util.impl;

import com.jam.pojo.Resp;
import com.jam.util.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;

/**
 * @program: sportplay
 * @description: 文件上传工具类
 * @author: Mr.Pu
 * @create: 2021-05-14 21:24
 **/

@Service
public class FileUploadService implements FileUpload {

    @Autowired
    FileUpload fileUpload;

    @Override
    public Resp<String> upload(MultipartFile file) {
        //判断文件是否为空
        if(file.isEmpty()){
            return Resp.fail("400","文件为空!");
        }
        //获取文件原始文件名
        String originalFilename = file.getOriginalFilename();
        if(!originalFilename.endsWith(".xls")){
            return Resp.fail("300","文件必须为excel格式文件");
        }
        //生成自己的文件名 系统时间+后缀扩展名
        String fileName = System.currentTimeMillis()+"."+originalFilename.substring(originalFilename.lastIndexOf("."));
        //文件保存路径
        String filePath = "F:\\WebProject\\sportplay\\src\\main\\resources\\static\\UploadedFile";
        //创建文件
        File dest = new File(filePath);
        if(!dest.exists()){
            dest.mkdirs();
            try{
                file.transferTo(new File(dest,fileName));
            }catch (Exception e){
                e.printStackTrace();
                return Resp.fail("500",originalFilename+"上传失败");
            }
        }
        return Resp.success(fileName);
    }
}
