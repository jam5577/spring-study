package com.jam.controller;

import com.alibaba.fastjson.JSON;
import com.jam.dao.MenuDao;
import com.jam.pojo.MainMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
public class MenuController {

    @Autowired
    MenuDao menuDao;

    @RequestMapping("/menu")
    public String getAllMenus(){
        HashMap<String,Object> data = new HashMap<>();
        List<MainMenu> menu = menuDao.getMenu();
        if(menu!=null){
            data.put("menu",menu);
            data.put("flag",200);
        }else {
            data.put("flag",404);
        }
        return JSON.toJSONString(data);
    }
}
