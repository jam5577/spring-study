package com.jam.dao;

import com.jam.pojo.Component;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentDao {

    List<Component> getAllComponent(@Param("cname") String cname, @Param("pageStart") int PageStart, @Param("pageSize") int pageSize);

    int getComponentCounts(@Param("cname") String cname);

    int addComponent(Component component);

    int deleteComponent(int id);

    Component updateComponent(int id);

    int editComponent(Component component);

}
