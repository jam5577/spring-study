package com.jam.dao;

import com.jam.pojo.Charts;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChartsDao {

    List<Charts> getAllData();
}
