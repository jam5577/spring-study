package com.jam.util;

import com.jam.pojo.Resp;
import org.springframework.web.multipart.MultipartFile;

public interface FileUpload {

    Resp<String> upload(MultipartFile file);
}
