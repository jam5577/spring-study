package com.jam.controller;

import com.alibaba.fastjson.JSON;
import com.jam.dao.ComponentDao;
import com.jam.pojo.Component;
import com.jam.pojo.QueryInfo;
import com.jam.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;


/**
 * @program: sportplay
 * @description: 零件管理控制层
 * @author: Mr.Pu
 * @create: 2021-05-23 16:15
 **/

@RestController
public class ComponentController {

    @Autowired
    ComponentDao componentDao;

    /**
     * @Description: 查询所有的零件，支持模糊查询，使用前端回传的页码数据进行统计总个数，使用List存放所有零件，并使用HashMap存放数据，返回JSON
     * @param queryInfo
     * @Return: java.lang.String
     * @Author: Mr.Pu
     * @Date: 2021/5/23 
     */
    @RequestMapping("/allComponent")
    public String getComponentList(QueryInfo queryInfo){
        int numbers = componentDao.getComponentCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();
        List<Component> components = componentDao.getAllComponent("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", components);
        return JSON.toJSONString(res);
    }

    /**
     * @Description: 查询当前更新的零件
     * @param id
     * @Return: java.lang.String
     * @Author: Mr.Pu
     * @Date: 2021/5/23
     */
    @RequestMapping("/updateComponent")
    public String updateComponent(int id){
        Component component = componentDao.updateComponent(id);
        return JSON.toJSONString(component);
    }

    /**
     * @Description: 更新所选的零件
     * @param component
     * @Return: java.lang.String
     * @Author: Mr.Pu
     * @Date: 2021/5/23 
     */
    @RequestMapping("/editComponent")
    public String editComponent(@RequestBody Component component){
        int i = componentDao.editComponent(component);
        return i>0 ? "success":"error";
    }

    /**
     * @Description: 添加新零件
     * @param component
     * @Return: java.lang.String
     * @Author: Mr.Pu
     * @Date: 2021/5/23 
     */
    @RequestMapping("/addComponent")
    public String addComponent(@RequestBody Component component){
        component.setCstate(1);
        int i = componentDao.addComponent(component);
        return i>0 ? "success" : "error";
    }
    @RequestMapping("/deleteComponent")
    public String deleteComponent(int id){
        int i = componentDao.deleteComponent(id);
        return i>0 ? "success" : "error";
    }
}
